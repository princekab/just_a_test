package login;

import base.BasePage;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

public class LoginPage extends BasePage {

    //constructor
    public LoginPage(WebDriver driver) {
        super(driver);
    }

    /* identifying web elements */

    // user name text box web element
    private static final By userNameTextBox = By.name("userName");

    // password text box web element
    private static final By passwordTextBox = By.name("password");

    // submit button web element
    private static final By submitButton = By.name("login");

    // register link web element
    private static final By registrationFormLink = By.xpath("//a[contains(text(),'registration')]");

    // copy right web element
    private static final By copyRightText = By.xpath("//div[@class='footer']");


    // login by providing a user and a password
    public void loginUsingUsrPass(String userName, String password) {

        // enter a text to the user name text box
        enterAText(userName, userNameTextBox);
        // enter a text to the user name text box
        enterAText(password, passwordTextBox);
        // enter a text to the user name text box
        click(submitButton);
    }

    // click on register form link
    public void clickOnRegisterFormLink() {
        click(registrationFormLink);
    }

    // get copy right text
    public String readCopyRightText() {
        String text = readText(copyRightText);
        System.out.println("The copy right text is: \n" + text);
        return text;
    }
}
