package base;

import io.github.bonigarcia.wdm.WebDriverManager;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;

public class TestBase {

    protected WebDriver driver;

    @BeforeTest
    public void setupTest() {
        WebDriverManager.chromedriver().setup();
        driver = new ChromeDriver();

        enterUrl("http://newtours.demoaut.com/mercurysignon.php");
    }

    @AfterTest
    public void teardown() {
        if (driver != null) {
            driver.quit();
        }
    }

    // get page title
    public String getPageTitle() {
        return driver.getTitle();
    }

    // enter a url
    public void enterUrl(String url) {
        // enter a URL
        driver.get(url);
    }

    // read the configuration file
    public String readConfigFile(String key) {
        return "";
    }

    // wait in milsecs
    public void waitInMilSecs(int time) {
        try {
            Thread.sleep(time);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }
}
