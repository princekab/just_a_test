package base;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

public class BasePage {

    protected WebDriver driver;

    public BasePage(WebDriver driver) {
        this.driver = driver;
    }

    // send keys
    public void enterAText(String text, By webElement) {
        driver.findElement(webElement).sendKeys(text);
    }

    // click
    public void click(By webElement) {
        driver.findElement(webElement).click();
    }

    // read text
    public String readText(By webElement) {
        return driver.findElement(webElement).getText();
    }
}
