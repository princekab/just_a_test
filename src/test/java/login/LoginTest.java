package login;

import base.TestBase;
import org.testng.Assert;
import org.testng.annotations.Test;

public class LoginTest extends TestBase {

    // Validate that the copy right text is correctly displayed
    @Test(enabled=true, priority=0)
    public void validateTheCopyRightText() {

        // create login page object
        LoginPage login = new LoginPage(driver);

        String expectedResult = "© 2005, Mercury Interactive (v. 011003-1.01-058)";
        String actualResult;

        actualResult = login.readCopyRightText();

        // Validation
        Assert.assertEquals(actualResult, expectedResult);
    }

    // Validate that the copy right text is correctly displayed
    @Test(enabled=true, priority=1)
    public void validateThatRegitrationFormWorks() {

        // create login page object
        LoginPage login = new LoginPage(driver);

        login.clickOnRegisterFormLink();
        waitInMilSecs(5000);

        Assert.assertTrue( getPageTitle().equals("Register: Mercury Tours"), "Error!!! Registration form is not working");
    }

}
